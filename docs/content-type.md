## kingu.content-type

### [parameter] `content-type-db-file`

default to "/etc/mime.types"

### [parameter] `mailcap-file`

default to "/etc/mailcap"

### [function] `content-type-of`

```scheme
(content-type-of "/etc/passwd")
==> (text plain)
```
