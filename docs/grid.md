## kingu.grid

### [function] `make-grid`

```scheme
(define G (make-grid 5 5 #\x))
==>#<closure ((make-grid make-grid) req . args)>

(G 'get 3 2)
==> #\x

(G 'set 3 2 'y)
==> #<undef>

(G 'get 3 2)
==> y

(G 'width)
==> 5

(G 'height)
==> 5

(G 'vector)
==> #(#\x #\x #\x #\x #\x #\x #\x #\x #\x #\x #\x #\x #\x y #\x #\x #\x #\x #\x #\x #\x #\x #\x #\x #\x)
```

### [function] grid-ref & grid-set!

```scheme
(define G (make-grid 5 5 'x))
==> G

(grid-set! G 4 3 "patate")
==> #<undef>

(grid-ref G 4 3)
==> "patate"
```

### [function] grid-for-each

args:
* grid
* function to execute for each cell. arg: the cell content.
* function to execute at the begining of each line. arg: the line in a list.
* function to execute at the end of each line. arg: the line in a list.

```scheme
(define G (make-grid 3 3 'x))
==> G

(grid-for-each G (lambda (x)(display x))
                 (lambda (y)(display y)(display ": "))
		 (lambda (y)(display " :")(display y)(newline)))
(x x x): xxx :(x x x)
(x x x): xxx :(x x x)
(x x x): xxx :(x x x)
==> #<undef>

gosh> (grid-set! G 1 1 'o)
#<undef>

(grid-for-each G (lambda (x)(display x))
                 (lambda (y)(display y)(display ": "))
		 (lambda (y)(display " :")(display y)(newline)))
(x x x): xxx :(x x x)
(x o x): xox :(x o x)
(x x x): xxx :(x x x)
==> #<undef>
```

### [function] grid-for-each-with-index
  
args:
* grid
* function to execute for each cell. args: x-coor, y-coor, the cell content.
* function to execute at the begining of each line. args: the line number, the the line in a list.
* function to execute at the end of each line. args: the line number, the the line in a list.

```scheme
(define G (make-grid 3 3 'x))
==> G

(grid-for-each-with-index G (lambda (x y v) (display v))
                            (lambda (x v) (display x)(display "-"))
			    (lambda (y v) (newline)))
0-xxx
1-xxx
2-xxx
==> #t
```

### [function] `grid-copy`

Return a copy of the grid

```scheme
(define G1 (make-grid 3 3 'x))
==> G1

(define G2 (grid-copy G1))
==> G2
```

### [function] `grid-display`

```scheme
(define G (make-grid 3 3 'x))
==> G

(grid-display G)
 x  x  x 
 x  x  x 
 x  x  x 
==> #<undef>
```

### [function] `vov->grid`

```scheme
(grid-display
   (vov->grid (vector (vector 1 2 3)
                      (vector 4 5 6)
		      (vector 7 8 9))))
 1  2  3 
 4  5  6 
 7  8  9 
==> #<undef>
```
