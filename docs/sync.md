## kingu.sync

### [function] `sync`

```scheme
(sync local-dir user@remote-host:remote-dir)
==> #<undef>
```

Will sync local-dir to remote-dir.
i.e. file will be transfered from local to remote if absent or different on remote.

### [function] `sync-both-way`

```scheme
(sync-both-way dir1 dir2)
==> #<undef>
```

Will invoke (sync dir1 dir2) then (sync dir2 dir1)

