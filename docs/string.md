## kingu.string

### [function] `remove-from-string`

```scheme
(remove-from-string "patate" #\a #\t)
==> "pe"
```

### [function] `remove-diacritic-from-string`

```scheme
(remove-diacritic-from-string "hétérogénéité")
==> "heterogeneite"
```

### [function] `remove-leading-article`

```scheme
(remove-leading-article "The Beatles")
==> "Beatles"

(remove-leading-article "Les Sheriff")
==> "Sheriff"
```

### [function] `string->hashtag`

```wcheme
(string->hashtag "Punk Rock")
==> "#PunkRock"
```