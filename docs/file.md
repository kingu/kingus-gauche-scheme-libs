## kingu.file

### [function] `sanitize-path`

```scheme
(sanitize-path "/home/kingu/mes fichiers")
==> "_2Fhome2Fkingu2Fmes20fichiers"
```

### [function] `recursive-directory-list`

Assuming this file structure:

```
/tmp --> /example --> /dir1 --> file1.jpg
                  |
                  --> /dir2 --> file2.txt
		            |
			    --> /dir3 --> file3.xml
```

```scheme
(recursive-directory-list "/tmp/example/")
==> ("/tmp/example/dir1/file1.jpg" "/tmp/example/dir2/dir3/file3.xml" "/tmp/example/dir2/file2.txt")
```

### [function] `string->file'

```scheme
(file-exists? "/tmp/patates")
==> #f

(string->file "C'est bon les patates" "/tmp/patates")
==> "/tmp/patates"

(file-exists? "/tmp/patates")
==> #t

(file->string "/tmp/patates")
==> "C'est bon les patates"
```

### [function] file-for-each

```scheme
(file-for-each print "/etc/fstab")
proc            /proc           proc    defaults          0       0
/dev/mmcblk0p1  /boot           vfat    defaults          0       2
/dev/mmcblk0p2  /               ext4    defaults,noatime  0       1
==> #<undef>
```

### [function] `file/path'

Join path/file, taking care of the trailing / in path

```scheme
(file/path "/home/kingu" "file.scm")
==> "/home/kingu/file.scm"

(file/path "/home/kingu/" "file.scm")
==> "/home/kingu/file.scm"
```
