## kingu.csv

### [function] `csv-foreach`

Args:
* csv-file <string>     - The csv file to parse
* func     <procedure>  - The procedure to invoque for each line

Keys:
* :before  <trunk>  - A procedure to invoque before the parsing
* :after   <trunk>  - A procedure to invoque after the parsing
* skip-first-line? <bool>  - If #t the first line of the file will be ignored (default #f)
