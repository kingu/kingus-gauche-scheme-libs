## kingu.timer

### [function]  with-timer

```scheme
(with-timer (lambda () (+ 3 3)))
Time: 0.1
==> 6

(with-timer (lambda () (+ 3 3)) :output (current-error-port))
Time: 0.1  <-- this is printed on stderr
==> 6

(with-timer (lambda () (+ 3 3)) :values? #t)
Time: 0.1
==> 0.1
==> 6

(with-timer (lambda () (+ 3 3)) :output #f :values? #t)
==> 0.1
==> 6

(with-timer (lambda () (+ 3 3)) :output #f :return-timer? #t)
==> 0.1

(with-timer (lambda () (+ 3 3)) :format-string "Execution time: ~a seconds\n")
Execution time: 0.1 seconds
==> 6
```
