## kingu.taffy

Interface to the taffy tag editor.
https://github.com/jangler/taffy

### [function] `taffy-read`

```scheme
(taffy-read (expand-path "~/music/Sex Pistols/[1977] Never Mind The Bollocks Here's The Sex Pistols/03 - No Feelings.mp3"))
==> ((album "Never Mind The Bollocks Here's The Sex Pistols")
     (artist "Sex Pistols")
     (genre "Punk Rock")
     (title "No Feelings")
     (track "3")
     (year "1977"))
```

### [function] `taffy-write`

```scheme
(define file (expand-path "~/music/Compilations/VA - Punk En France Vol.1/Punk En France (CD1)/01 STARSHOOTER - Betsy Party.mp3"))
==> file

(taffy-read file)
==> ((album "Punk en France Vol.1 CD1") (genre "Punk Rock") (year "2003"))

(taffy-write! file '((artist "Starshooter")(title "Betsy Party")))
==> #<process 3203 "taffy" active>

(taffy-read file)
==> ((album "Punk en France Vol.1 CD1") (artist "Starshooter") (genre "Punk Rock") (title "Betsy Party") (year "2003"))
``` 

