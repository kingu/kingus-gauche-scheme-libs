## kingu.char

### [function] `strip-diacritic`

```scheme
(strip-diacritic #\é)
==> #\e
```