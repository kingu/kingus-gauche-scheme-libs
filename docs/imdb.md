## kingu.imdb

### [Parameter] `http-server`

The server name.  Default: www.myapifilms.com

### [Parameter] `api-token`
Your personal api token. No default, must be set.

### [Parameter] `imdb:xml-directory`

A directory to cache xml files.  Must be accessible for r/w.
No default, must be set.

### [class] `<movie>`

* id
* title
* countries
* actors
* directors
* type 
* genres
* ratings
* xml

### [function] `imdb:id->movie`

```scheme
(define movie (imdb:id->movie "tt0076162"))
=> movie

(~ movie 'title)
==> "Hausu"
```

