## kingu.countries

### [variable] `countries`

This variable is a list where each element consist of:

* the country name (in english)
* the ISO 2 letters code
* the ISO 3 letters code
* the ISO numeric code
* the "liftweb" code ("liftweb" is a Scala library)

```scheme
(assoc "Canada" countries)
==> ("Canada" CA CAN 124 32)
```
