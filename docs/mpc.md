## kingu.mpc

### [parameter] `mpc-host`

The mpd server hostname.  Default to "localhost"

### [parameter] `mpc-port`

The mpd server port number.  Default to "6600"

### [function] is-mpd-playing?

no args.  return #t if mpd is playing or #f if it is paused.

### [function] mpc

Issue an mpc command and return the result in a list.

```scheme
(mpc 'play)
==> ("Gentle Giant - Isn't It Quiet And Cold?"
     "[playing] #1/3   3:21/3:52 (86%)"
     "volume: 93%   repeat: off   random: off   single: off   consume: on ")

(mpc 'search 'artist "Jon Lord")
==> ("Jon Lord/1976 - Sarabande/01-Fantasia.mp3"
     "Jon Lord/1976 - Sarabande/02-Sarabande.mp3"
     "Jon Lord/1976 - Sarabande/03-Aria.mp3"
     ...)

(mpc 'playlist)
==> ("Die Mimmi's - Fanclub Präsident Banane")

(mpc 'add "Jon Lord/1976 - Sarabande/01-Fantasia.mp3") 
==> ()

(mpc 'playlist)
==> ("Die Mimmi's - Fanclub Präsident Banane"
     "Jon Lord - Fantasia")

### [function] `mpc-update`

Update the mpd database and wait for the update to finish.
To update without waiting, use `(mpc 'update)

### [function] `mpc-idle`

```scheme
(mpc-idle 'player 'playlist)
==> "player"
``` 