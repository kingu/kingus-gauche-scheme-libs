## kingu.id3v2

Note:  The program 'id3v2' must be installed.
       http://id3v2.sourceforge.net/

### [class] `<id3v2>`

* filename
* artist
* title
* album
* genre
* year

### [function] `id3v2`

```scheme
(define tags (id3v2 "/home/grpmpdp/music/Merzbow/(1995) Magnesia Nova/01-Rituale Lucis.mp3"))
==> tags

(~ tags 'artist)
==> "Merzbow"

(~ tags 'genre)
==> "Noise"
```
