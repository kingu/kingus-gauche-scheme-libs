## kingu.type-convert

### [method] `->string`, `->symbol` & `->number`

```scheme
(->string 34)
==> "34"

(->string #t)
==> "#t"

(->symbol "patate")
==> patate

(->symbol 42)
|42|

(->number "42")
==> 42

(->number #f)
==> 0

(->number #t)
==> 1

(->number "patate")
==> 0
```
