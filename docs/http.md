## kingu.http

### [class] `<http-client-request>`

* server :init-keyword :server :init-value "localhost"
* port   :init-keyword :port :init-value 80
* query  :init-keyword :query :init-value "/"
* method :init-keyword :method :init-value 'get
* file   :init-keyword :file :init-value #f

Note: A <http-client-request> object self evaluate to a <http-server-reply> object.

```scheme
(define request (make <http-client-request> :server "www.google.com"))
==> #<<http-client-request> 0x2250990>

(request)
==> #<<http-server-reply> 0x22556d0>
```

### [class] `<http-server-reply>`

* code
* head
* data

Note: A <http-server-reply> object self evaluate to it's data.

```scheme
(define request (make <http-client-request> :server "www.google.com"))
==> #<<http-client-request> 0x2250990>

(define reply (request))
==> #<<http-server-reply> 0x22556d0>

(~ reply code)
==> "200"

(~ reply 'head)
==> (("date" "Sat, 01 Sep 2018 14:51:47 GMT") ("expires" "-1") ("cache-control" "private, max-age=0") ("content-type" "text/html; charset=ISO-8859-1") ("p3p" "CP=\"This is not a P3P p\olicy! See g.co/p3phelp for more info.\"") ("server" "gws") ("x-xss-protection" "1; mode=block") ("x-frame-options" "SAMEORIGIN") ("set-cookie" "1P_JAR=2018-09-01-14; expires=Mon,\ 01-Oct-2018 14:51:47 GMT; path=/; domain=.google.com") ("set-cookie" "NID=138=M3YU6y4IYs3xwqdPIDkMmt03Q-pdhmcIgd63MxTx0UWrAVEM-upeDMmbxABImT6950euK9yEwU5D1FbW4dTkZin3XoXubw2qgo2n\IXxHuYjxZZLvptndN-2vpwWtd8LS; expires=Sun, 03-Mar-2019 14:51:47 GMT; path=/; domain=.google.com; HttpOnly") ("accept-ranges" "none") ("vary" "Accept-Encoding") ("transfer-encoding\" "chunked"))

(reply)
==> "<html>...</html>"
```

### [method] `send-query`

```scheme
(define request (make <http-client-request> :server "www.google.com"))
==> #<<http-client-request> 0x2250990>

(send-query request)
==> #<<http-server-reply> 0x22556d0>
```

### [function] `uri->string`

```scheme
(uri->string "http://www.foo.com")
==> "<html>...</html>
```

