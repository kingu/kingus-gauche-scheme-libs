## kingu.process

### [function] `process->list`

```scheme
(process->list  "ps")
==> ("  PID TTY          TIME CMD"
     "18377 pts/2    00:00:02 gosh"
     "18611 pts/2    00:00:00 sh"
     "18612 pts/2    00:00:\01 ps")
```

### [function] `process->string`

```scheme
(process->string  "ps")
==> "  PID TTY          TIME CMD\n18377 pts/2    00:00:02 gosh\n18642 pts/2    00:00:00 sh\n18644 pts/2    00:00:01 ps"
```
