## kingu.pprint

### [function] `pprint`

```scheme
(pprint '((a b) c (d (e) f)))
((a b)
 c
 (d
  (e)
  f))
==> #<undef>
```
