## kingu.time

### [method] decompose-seconds

```scheme
(decompose-seconds 300000)
==> "3 days 11 hours 20 minutes & 0 seconds\n"

(decompose-seconds 300000 "~a days ~a heures ~a minutes & ~a secondes\n")
==> "3 days 11 heures 20 minutes & 0 secondes\n"
```
	