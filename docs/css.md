## kingu.css

### [function] `css`

```scheme
(css '((color "blue")
       (width "50px")))
==> "color:blue;width:50px"
```

### [function] `css-rule`

```scheme
(css-rule "patate"  '((color "blue")(width "50px")))
==> "patate { color:blue;width:50px }\n"
```

### [function] css-stylesheet

```scheme
(stylesheet '(("#patate" ((color "blue")
                          (width "50px")))
              ("#navet" ((width "100px")
                         (height "125px")))))
==> ("#patate { color:blue;width:50px }\n" "#navet { width:100px;height:125px }\n")