## kingu.list

### [function] `flatten`

```scheme
(flatten '(a (b) (c (d e)((f)))))
==> (a b c d e f)
```

### [function] `split-every-nth`

```scheme
(split-every-nth '(a b c d e f) 2)
==> ((a b)(c d)(e f))
```

### [function] `make-range`

```scheme
(make-range 3 8)
==> (3 4 5 6 7 8)
```

