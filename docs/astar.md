## kingu.astar

### [function] `a*`

```scheme
(define maze (make-grid 5  5 '1))
==> maze

(grid-set! maze 1 0 0)
(grid-set! maze 1 1 0)
(grid-set! maze 1 2 0)
(grid-set! maze 1 3 0)
(grid-set! maze 3 2 0)
(grid-set! maze 3 3 0)
(grid-set! maze 3 4 0)

(grid-display maze)
 1  0  1  1  1 
 1  0  1  1  1 
 1  0  1  0  1 
 1  0  1  0  1 
 1  1  1  0  1 

(define path (a* maze '(0 . 0) '(4 . 4)))

 path
==> ((0 . 0) (0 . 1) (0 . 2) (0 . 3) (0 . 4) (1 . 4) (2 . 4) (2 . 3) (2 . 2) (2 . 1) (3 . 1) (4 . 1) (4 . 2) (4 . 3) (4 . 4))

(for-each (lambda (node) (grid-set! maze (car node)(cdr node) 'X)) path)

(grid-display maze)
 X  0  1  1  1 
 X  0  X  X  X 
 X  0  X  0  X 
 X  0  X  0  X 
 X  X  X  0  X 
```

#### Options

A* can be customized with a few options

##### heuristic

```scheme
(a* maze '(0 . 0) (9 . 9) :heuristic manhattan)
```

heuristic should be function with 3 arguments: from, to and the value of a move in movement points.
This function shoult compute an estimation of the total number of movement points required for going from point FROM to point TO.

3 heuristics are provided: manhattan, euclidian and dijkstra.

###### [function] `manhattan`

D = (|from-x - to-x| + |from-x - to-y|) * MP

###### [function] `euclidian`

     ___________________________________
D = √(from-x - to-x)² + (from-y - to-y)² * MP

###### [function] dijkstra

D = 0

##### neighbours

neighbours should be a function of one argument: a node.
Given a node, it return a list of all adjacent nodes.
Default is ` (lambda (n) (get-neighbours-func n grid #f))`

Some auxiliary functions are provided.

###### [function] get-neighbours-func

Take 3 args: The node, the grid, and a boolean meaning if diagonal move are accepted.
A square grid is assumed.

```scheme
(get neighbour-func '(3 . 2) maze #f)
```

###### [function] get-neighbours-iter

Take 3 args: The node, the grid width, and the grid height.
Diagonals are always accepted.
A square grid is assumed.

```scheme
(get neighbour-iter '(3 . 2) 10 10)
```

###### [function] get-neighbours-hex

Take 3 args: The node, the grid width, and the grid height.
An hexagonal grid is assumed.

```scheme
(get neighbour-hex '(3 . 2) 10 10)
```

##### move

A function that return The basic movement point cost for moving from a node to an adjacent one.
Default is an internal function that retuen 10 for straight line and 14 for diagonals.
Future version of this lib should include the grid so you can have differend type of terrains.
args are from and to.

##### walkable

A function of 2 arguments: the grid and a node.
Return wether this node is walkable or not.

Default to an internal function that consider 0 to be a wall and 1+ to be walkable.

