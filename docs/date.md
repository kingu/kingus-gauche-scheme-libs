## kingu.date

### [function] `date-exists?`

```scheme
(date-exists? (make-date 0 0 0 0 12 12 2018 0))
==> #t
(date-exists? (make-date 0 0 0 0 12 13 2018 0))
==> #f
(date-exists? (make-date 0 0 0 0 29 2 2018 0))
==> #f
(date-exists? (make-date 0 0 0 0 29 2 2020 0))
==> #t
```

### [function] `next-day`

```scheme
(date->string (next-day (make-date 0 0 0 0 12 3 2020 0)))
==> "Fri Mar 13 00:00:00Z 2020"

(date->string (next-day (make-date 0 0 0 0 31 12 2020 0)))
==> "Fri Jan 01 00:00:00Z 2021"
```

### [function] `same-date`

```scheme
(same-date? (make-date 0 0 0 0 31 12 2020 0) (make-date 0 15 6 30 31 12 2020 0))
==> #t
```

### [function] `date-range`

```scheme
(map date->string (date-range (make-date 0 0 0 0 31 12 2020 0)
                              (make-date 0 0 0 0 3 1 2021 0)))
==> ("Thu Dec 31 00:00:00Z 2020" "Fri Jan 01 00:00:00Z 2021" "Sat Jan 02 00:00:00Z 2021" "Sun Jan 03 00:00:00Z 2021")
```
