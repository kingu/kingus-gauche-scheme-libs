(define-module kingu.taffy
  (use srfi-13)
  (use kingu.type-convert)
  (use gauche.process)
  (use kingu.list)
  (use kingu.process)
  (export-all))
(select-module kingu.taffy)

;; This is an interface to the taffy tag editor.
;; https://github.com/jangler/taffy

(define taffy-read
  (lambda (file)
    (filter-map (lambda (x)
	   (let ((z (string-split x #\:)))
	     (if (null? (cdr z))
		 #f
		 (list (string->symbol (car z))
		       (string-trim (cadr z))))))
	 (cdr (process->list `(taffy ,file))))))

(define taffy-write!
  (lambda (file tags)
    (run-process (flatten `(taffy ,file ,@(map (lambda (y) (list (string-append "--" (->string (car y))) (cadr y))) tags))) :wait #t)))



