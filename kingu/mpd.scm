(define-module kingu.mpd
  (use gauche.parameter)
  (use gauche.net)
  (use kingu.process)
  (use srfi-13)
  (use rfc.json)
  (export-all))

(select-module kingu.mpd)

(define mpd-port (make-parameter 6600))
(define mpd-host (make-parameter "localhost"))
(define mpd-on-ack (make-parameter #f))

(define mpd:connect
  (lambda ()
    (make-client-socket 'inet (mpd-host) (mpd-port))))


(define-class <track> () 
  ((file :init-keyword :file)
   (Artist :init-value "Unknown")
   (Title :init-value "Unknown")
   (Last-Modified)
   (Time)
   (AlbumArtist)
   (ArtistSort)
   (AlbumArtistSort)
   (Album :init-value "Unknown")
   (AlbumSort)
   (Date)
   (Disc)
   (Track)
   (Composer)
   (Genre :init-value "unset")
   (MUSICBRAINZ_ALBUMARTISTID)
   (MUSICBRAINZ_ALBUMID)
   (MUSICBRAINZ_ARTISTID)
   (MUSICBRAINZ_TRACKID)
   (playlist)
   (Performer)
   (Pos)
   (Id)))

(define parse-mpd-result-list
  (lambda (l)
    (letrec ((obj #f)
	     (parser (lambda (lx objl)
		       (if (null? lx) 
			   objl
			   (if (eq? (caar lx) 'file)
			       (parser (cdr lx) (cons (make <track> :file (cdar lx)) objl))
			       (begin
				 (slot-set! (car objl) (caar lx) (cdar lx))
				 (parser (cdr lx) objl))
			       )))))
      (parser l (list)))))


(define mpd
  (lambda (cmd)
    (call-with-current-continuation 
     (lambda (k)
       (letrec ((buf '())
		(loop (lambda (p) 
			(let ((line (read-line p)))
			  
			  (when (#/^ACK/ line)
				(if (mpd:on-ack) 
				    ((mpd:on-ack) line k)
				    (k #f)))
			  
			  (if (string=? "OK" line)
			      (reverse buf)
			      (begin					      
				(unless (#/^OK MPD/ line)
					(push! buf (let ((spl (string-split line #\:)))
						     (cons (string->symbol (car spl))
							   (string-trim-both (cadr spl))))))
				(loop p)))))))
	 
	 (call-with-client-socket (mpd:connect)
				  (lambda (in out)
				    (unless out (error out))
				    (unless in (error in))
				    (format out "~a\n" cmd)
				    (loop in))))))))
     
    
  


				      
