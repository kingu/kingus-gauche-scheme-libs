(define-module kingu.mpc
  (use gauche.process)
  (use kingu.process)
  (use gauche.parameter)
  (export mpc is-mpd-playing? mpc-update mpc-idle mpc-update-track))

(select-module kingu.mpc)

(define mpc-host (make-parameter "localhost"))
(define mpc-port (make-parameter "6600"))

(define mpc
  (lambda (cmd . args)
    (process->list `(mpc -h ,(mpc-host) -p ,(mpc-port) ,cmd ,@args ))))
  
(define is-mpd-playing?
  (lambda ()
    (let ((x (process->string "mpc | grep '\\[playing\\]'")))
      (if (string=? x "") #f #t))))

(define mpc-update
  (lambda ()
    (run-process `(mpc -h ,(mpc-host) -p ,(mpc-port) update --wait) :wait #t)))

(define mpc-update-track
  (lambda (track)
    (run-process `(mpc -h ,(mpc-host) -p ,(mpc-port) update ,track --wait) :wait #t :output '/dev/null)))


(define mpc-idle
  (lambda args
    (process->string `(mpc -h ,(mpc-host) -p ,(mpc-port) idle ,@args))))
	  
