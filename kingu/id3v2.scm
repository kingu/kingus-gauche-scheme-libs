(define-module kingu.id3v2
  (use kingu.process) 
  (use srfi-13)
  (use file.util)
  (export-all))
(select-module kingu.id3v2)

;; This lib do not manage tag by itsel but is a frontend to the id3v2 program  (http://id3v2.sourceforge.net/)
;; It needs to be installed


(define-class <id3v2> ()
  ((filename :init-keyword :filename :init-value #f)
   (artist :init-value #f)
   (title :init-value #f)
   (album :init-value #f)
   (genre :init-value #f)
   (year :init-value #f)))


(define id3v2 
  (lambda (file)
    (unless (and (file-exists? file)
		 (string=? (path-extension file) "mp3"))
	    (error "Invalid file provided"))
    (let ((tag (make <id3v2> :filename file))
	  )
      
      (map 
       (lambda (x)
	 (rxmatch-cond
	  ((rxmatch #/^TIT2: (.*)/ x)
	   (#f title)
	   (slot-set! tag 'title title))
	  ((rxmatch #/^TALB: (.*)/ x)
	   (#f album)
	   (slot-set! tag 'album album))	 
	  ((rxmatch #/^TPE1: (.*)/ x)
	   (#f artist)
	   (slot-set! tag 'artist artist)) 
	  ((rxmatch #/^TYER: (.*)/ x)
	   (#f year)
	   (slot-set! tag 'year year))	 
	  ((rxmatch #/^TCON: (.*) \([0-9]+\)/ x)
	   (#f genre)
	   (slot-set! tag 'genre genre))	 
	  ))
       
       (process->list `(id3v2 -R ,file)))
      tag)))
  

