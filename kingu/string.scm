(define-module kingu.string
  (use kingu.char)
  (use gauche.collection)
  (export remove-from-string remove-diacritic-from-string remove-leading-article string->hashtag))
(select-module kingu.string)

(define remove-from-string
  (lambda (str . chars)
    (list->string (remove (lambda (c) (member c chars)) str))))

(define remove-diacritic-from-string 
  (lambda (s)
    (list->string (map strip-diacritic s))))

(define remove-leading-article
  (lambda (x)
    (let* ((match (#/\(([^\)]+)/ x))
	   (str (if match (rxmatch-substring match 1) x)))
      (rxmatch-cond
       ((rxmatch #/^The (.*)$/ str) (#f str2) str2)
       ((rxmatch #/^L[eo]s (.*)$/ str) (#f str2) str2)
       ((rxmatch #/^L[ae] (.*)$/ str) (#f str2) str2)
       ((rxmatch #/^Die (.*)$/ str) (#f str2) str2)
       (else str)))))

(define string->hashtag
  (lambda (str)
    (string-append "#" (remove-from-string (remove-diacritic-from-string str) #\space #\- #\_))))
